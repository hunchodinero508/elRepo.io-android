/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of ui;

class BrowseXScreen extends StatefulWidget {

  final Map<String, Widget> myTabs;
  final Function(TabController) listener;

  const BrowseXScreen ({Key key, @required this.myTabs, this.listener}) : super(key: key);

  @override
  _BrowseXScreenState createState() => _BrowseXScreenState();
}

class _BrowseXScreenState extends State<BrowseXScreen>
    with SingleTickerProviderStateMixin {

  TabController _tabController;

  final List<Tab> myTabs = [];
  final List<Widget> myWidgets = [];
  @override
  void initState() {
    widget.myTabs.forEach((key, value) {
      myTabs.add(new Tab(text: key));
      myWidgets.add(value);
    });
    _tabController =
      new TabController(vsync: this, length: myTabs.length);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: myTabs.length,
      child: Builder(builder: (BuildContext context) {
        final TabController _tabController = DefaultTabController.of(context);
        if (widget.listener != null) {
          _tabController.addListener(() {
            widget.listener(_tabController);
          });
        }

        return new Scaffold(
          appBar:new AppBar(
            toolbarHeight: 58,
            backgroundColor: Theme.of(context).backgroundColor,
            bottom: new TabBar(
              controller: _tabController,
              indicatorColor: REMOTE_COLOR,
              labelColor: LOCAL_COLOR,
              unselectedLabelColor: Colors.grey,
              //labelPadding: Vx.m4,
              tabs: myTabs,
            ),

          ),
          body: new TabBarView(
            controller: _tabController,
            children: myWidgets,
          ),
        );
      }),
    );
  }
}
