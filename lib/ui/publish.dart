/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/// lib/ui/publish.dart
part of ui;

class PublishScreen extends StatelessWidget {
  static Route<dynamic> route () => MaterialPageRoute(
    builder: (context) => PublishScreen(),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: WHITE_COLOR,
      body: new Container(
        margin: defaultMargin(),
        child: new ListView(children: <Widget>[new PublishForm()]),
      ),
    );
  }
}

class PublishForm extends StatefulWidget {
  PublishForm({Key key}) : super(key: key);

  @override
  _PublishFormState createState() => _PublishFormState();
}

class _PublishFormState extends State<PublishForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    models.PostData postData = new models.PostData();
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextFormField(
              decoration: InputDecoration(labelText: IntlMsg.of(context).title),
              validator: (value) {
                if (value.isEmpty)
                  return IntlMsg.of(context).titleCannotBeEmpty;
                postData.metadata.title = value;
                return null;
              },
            ),
            TextFormField(
              maxLines: 10,
              decoration: InputDecoration(
                  alignLabelWithHint: true,
                  labelText: IntlMsg.of(context).description),
              validator: (value) {
                postData.mBody.text = value;
                return null;
              },
            ),
            SizedBox(
              height: 16,
            ),
            RaisedButton.icon(
              onPressed: () async {
                final params = fileDialog.OpenFileDialogParams(localOnly: true);
                postData.filePath = await fileDialog.FlutterFileDialog.pickFile(params: params);
                postData?.filePath != null ?
                  postData.filename = path.basename(postData.filePath) :
                  postData.filename = "" ;
              },
              label: Text(IntlMsg.of(context).chooseFile),
              icon: Icon(Icons.upload_rounded),
              color: ACCENT_COLOR,
              textColor: WHITE_COLOR
              ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(top: 16),
                child: RaisedButton(
                  onPressed: () async {
                    FocusScopeNode currentFocus = FocusScope.of(context);
                    if (!currentFocus.hasPrimaryFocus) {
                      currentFocus.unfocus();
                    }
                    final form = _formKey.currentState;
                    if (form.validate()) {
                      form.save();
                      print(
                          "Post form received file path: ${postData.filePath}");
                      showLoadingDialog(
                          context, IntlMsg.of(context).publishingContent);

                      repo.publishPost(postData).then((publishData) {
                        form.reset();
                        postData = new models.PostData();

                        Navigator.pop(context); //pop dialog
                        final snackBar = SnackBar(
                          content: Text(IntlMsg.of(context).contentPublished),
                          duration: Duration(seconds: 6),
                          action: SnackBarAction(
                            label: IntlMsg.of(context).checkItOut,
                            onPressed: () {
                              Navigator.pushNamed(context, "/postdetails",
                                  arguments: PostArguments(publishData.forumId,
                                      publishData.msgId, false));
                            },
                          ),
                        );
                        Scaffold.of(context).showSnackBar(snackBar);
                        print(
                            "post Published on forum ${publishData.forumId}\n with id: ${publishData.msgId}\n and tags: ${publishData.hashTags}");
                      });
                    }
                  },

                  child: Center(child: Text(IntlMsg.of(context).publish)),
                  color: LOCAL_COLOR,
                  textColor: Colors.white,
                ),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Divider(
              height: 2,
              color: Colors.grey,
            ),
            SizedBox(
              height: 16,
            ),
            Text(IntlMsg.of(context).youCanAddHashTags,
              style: TextStyle(
                color: REMOTE_COLOR,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
