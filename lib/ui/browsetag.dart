/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of ui;

class BrowseTagScreen extends StatefulWidget {
  final TagArguments args;
  BrowseTagScreen(this.args);

  @override
  _BrowseTagScreenState createState() => _BrowseTagScreenState();
}

class _BrowseTagScreenState extends State<BrowseTagScreen> {

  @override
  void initState() {
    repo.syncForums();
    super.initState();
  }

  Future<List<dynamic>> _getTagHeaders() =>
      repo.getPostHeaders("TAG", widget.args.tagName);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("elRepo.io",
        style: TextStyle(color: WHITE_COLOR,)),
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
      ),
      body: PostTeasersUpdatable(generator: _getTagHeaders, linkPosts: true),
    );
  }
}
