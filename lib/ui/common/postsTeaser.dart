/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/constants.dart';
import 'package:elRepoIo/ui/common/updatableFutureBuilder.dart';
import 'package:elrepo_lib/models.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../ui.dart';

class PostTeasersUpdatable extends StatefulWidget {
  final Function generator;
  final bool linkPosts;
  final UpdatabelFutureBuilderController controller;


  const PostTeasersUpdatable({
    Key key,
    this.generator,
    this.controller,
    this.linkPosts = false})
      : super(key: key);

  @override
  _PostTeasersUpdatable createState() => _PostTeasersUpdatable();
}

class _PostTeasersUpdatable extends State<PostTeasersUpdatable> {
  @override
  Widget build(BuildContext context) {
    return UpdatableFutureBuilder(
      controller: widget.controller,
      generator: widget.generator,
      callbackWidget: (context, data, updateFunction) =>
          postTeaserCard(context, data, linkPosts: widget.linkPosts),);
  }
}

Widget postTeaserCard (context, dynamic data, {bool linkPosts = false}) {
  PostMetadata postMetadata =  PostMetadata.fromJsonString(data["mMsgName"]);
  return Card(
    elevation: 1.0,
    margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
    child: Container(
      //decoration: BoxDecoration(color: Colors.white),
      child: ListTile(
          contentPadding:
          EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            decoration: new BoxDecoration(
                border: new Border(
                    right: new BorderSide(
                        width: 1.0, color: REMOTE_COLOR))),
/*
                      child: Icon(getContentTypeIcon(postMetadata.contentType),
                          color: getStatusColor(bookmarkedIds, data["mMsgId"])),
*/
            child:
            getPostIcon(postMetadata, data["mMsgId"], linkPosts),
          ),
          onTap: () {
            // if it is a linkPost, we need to redirect to the CONTENT forum post on click.
            Navigator.pushNamed(context, "/postdetails",
                arguments: PostArguments(
                    data["mGroupId"], data["mMsgId"], linkPosts));
          },
          title: Text(
              postMetadata.title != null ? postMetadata.title : ""),
          subtitle: Text(postMetadata.summary != null
              ? postMetadata.summary
              : "")),
    ),
  );
}

