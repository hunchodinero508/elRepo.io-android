/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
 
import 'package:elRepoIo/ui/common/updatableFutureBuilder.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

class SearchBar extends StatefulWidget {
  /// Text editing controller
  final TextEditingController teController;
  // Function to execute when search action is performed
  final Function searchAction;

  const SearchBar({Key key,
    this.teController,
    this.searchAction
  }): super(key: key);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar>
    with TickerProviderStateMixin {

  TextEditingController _teController;

  @override
  void initState() {
    _teController = widget.teController ?? TextEditingController();
//    _teController.addListener(_search); // todo: use this to implement search on text change
    super.initState();
  }

  @override
  void dispose() {
    _teController.dispose();
    super.dispose();
  }

  _searchAction(){
    print("text field: ${_teController.text}");
    if (widget.searchAction != null)
      widget.searchAction(_teController);
  }

  @override
  Widget build(BuildContext context) {
    return
      new TextField(
        controller: _teController,
        decoration: InputDecoration(
          prefixIcon: Icon(
              Icons.search_outlined,
              color: ACCENT_COLOR
          ),
          suffixIcon: IconButton(
            onPressed: _searchAction ,
            icon: Icon(
                Icons.send,
                color: REMOTE_COLOR
            ),
          ),
        )
      );
  }
}
