/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../intl.dart';
import '../../ui.dart';

class UpdatableFutureBuilder extends StatefulWidget {
  final Function generator;
  /// This function will return a Widget that show the AsyncSnapshot data
  final Function callbackWidget;
  final UpdatabelFutureBuilderController controller;

  const UpdatableFutureBuilder({
    Key key,
    this.generator,
    @required this.callbackWidget,
    this.controller
  })
      : super(key: key);

  @override
  _UpdatableFutureBuilder createState() => _UpdatableFutureBuilder(controller);
}

class _UpdatableFutureBuilder extends State<UpdatableFutureBuilder> {

  UpdatabelFutureBuilderController _controller;
  Future<List<dynamic>> _future;
  Function _generator;

  _UpdatableFutureBuilder(UpdatabelFutureBuilderController controller) {
    if(controller!=null) {
      controller.runFuture = _runFuture;
      _controller = controller;
    }
  }

  Future<List> _voidGenerator () async => await [];


  _runFuture([dynamicGenerator]) {
    _future =
      // Throught a controller you can pass a function dynamically as generator
      dynamicGenerator != null ? dynamicGenerator() :
          // Or else, use the generator defined on initState()
          _generator != null ? _generator( )
          // If generator is not defined, generate a void list
            : _voidGenerator();
    setState(() {});
  }

  @override
  void initState() {
    // Yo can define the generator using widget o a controller
    _generator = _controller?.generator ?? widget.generator;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    if (_future == null) {
      _runFuture();
    }

    return _ReRunnableFutureBuilder(
        _future,
        onRerun: _runFuture,
        callbackWidget: (context, data) => widget.callbackWidget(context, data, _runFuture)
    );
  }
}

class _ReRunnableFutureBuilder extends StatelessWidget {
  final Future<List<dynamic>> _future;
  final Function onRerun; // Function that run setState
  final Function callbackWidget; // builder caller where pass snapshot data  when no errors
  final _refreshKey = GlobalKey<RefreshIndicatorState>();

  _ReRunnableFutureBuilder(this._future, { this.onRerun, @required this.callbackWidget });

  Future<void> _pulldownRefresh(context) => onRerun();

  @override
  Widget build(BuildContext context) {
    return
      RefreshIndicator(
        key: _refreshKey,
        onRefresh:  () async => _pulldownRefresh(context),
        child: FutureBuilder(
          future: _future,
          builder: (context, AsyncSnapshot snapshot) {
            if (snapshot.connectionState != ConnectionState.done) {
              return loadingBox();
            }
            if (snapshot.hasError) {
              print('Error: ${snapshot.error}');
              return Center(
                child: Text(IntlMsg.of(context).cannotLoadContent),
              );
            }
            if (snapshot.data == null || snapshot.data.length == 0) {
              return Visibility(visible: false, child: Text(""));
            }
            return
                SizedBox(
                  width: 1000, // Trying to avoid Failed assertion 'constraints.hasBoundedWidth': is not true.  
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, index) {
                        final data = snapshot.data[index];
                        return callbackWidget(context, data,);
                      }
              ),
                );
          }),
      );
  }
}

class UpdatabelFutureBuilderController {
  void Function([Function dynamicGenerator]) runFuture;
  Future<List<dynamic>> Function() generator;
}
