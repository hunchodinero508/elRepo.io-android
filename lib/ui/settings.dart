/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of ui;

class SettingsProfile extends StatefulWidget {
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<SettingsProfile> {
  String valueChoose;
  List listItem = ["English", "Spanish", "Portuguese"];
  String locationName = "";

  @override
  void initState()  {
    repo.getLocation().then((location) {
      setState((){
        locationName = location["mLocationName"];
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
        elevation: 0,
        //iconTheme: IconThemeData(color: Colors.black),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.arrow_back_rounded,
            color: WHITE_COLOR,
          ),
        ),
        title: Text(
          'Settings',
          style: TextStyle(
            fontWeight: FontWeight.w800,
            color: WHITE_COLOR,
          ),
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Card(
                  elevation: 8.0,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: LOCAL_COLOR,
                  child: ListTile(
                    /*onTap: () {
                      Navigator.pushNamed(context, '/editprofile');
                    },*/
                    title: Text(
                      locationName,
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    leading: CircleAvatar(
                      backgroundImage: NetworkImage(
                          "https://thispersondoesnotexist.com/image"),
                    ),
                    /*trailing: Icon(
                      Icons.edit,
                      color: Colors.white,
                    ),*/
                  ),
                ),
                const SizedBox(height: 6.0),
                Column(
                  children: <Widget>[
                    ListTile(
                      leading: Icon(
                        Icons.file_download,
                        color: LOCAL_COLOR,
                      ),
                      title: Text("Your elRepo.io network"),
                      trailing: Icon(
                        Icons.keyboard_arrow_right_rounded,
                        color: LOCAL_COLOR,
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/friendnodes');
                      },
                    ),
                    _buildDivider(),
                    ListTile(
                      leading: Icon(
                        Icons.file_download,
                        color: LOCAL_COLOR,
                      ),
                      title: Text("Create a Backup"),
                      trailing: Icon(
                        Icons.keyboard_arrow_right_rounded,
                        color: LOCAL_COLOR,
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/createbackup');
                      },
                    ),
                    _buildDivider(),
                    ListTile(
                      leading: Icon(
                        Icons.translate,
                        color: LOCAL_COLOR,
                      ),
                      title: DropdownButton(
                        hint: Text("Choose a language"),
                        icon: Icon(
                          Icons.keyboard_arrow_down_rounded,
                          color: LOCAL_COLOR,
                        ),
                        isExpanded: true,
                        underline: SizedBox(),
                        value: valueChoose,
                        onChanged: (newValue) {
                          //setState((){
                          //  valueChoose = newValue;
                          //});
                        },
                        items: listItem.map((valueItem) {
                          return DropdownMenuItem(
                            value: valueItem,
                            child: Text(valueItem),
                          );
                        }).toList(),
                      ),
                      onTap: () {
                        //Navigator.pushNamed(context, '/language');
                      },
                    ),
                    _buildDivider(),
                    ListTile(
                      leading: Icon(
                        Icons.settings_remote,
                        color: LOCAL_COLOR,
                      ),
                      title: Text("Remote Control"),
                      trailing: Icon(
                        Icons.keyboard_arrow_right_rounded,
                        color: LOCAL_COLOR,
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/remotecontrol');
                      },
                    ),
                    _buildDivider(),
                    ListTile(
                      leading: Icon(
                        Icons.privacy_tip,
                        color: LOCAL_COLOR,
                      ),
                      title: Text("App intro"),
                      trailing: Icon(
                        Icons.keyboard_arrow_right_rounded,
                        color: LOCAL_COLOR,
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, '/onboarding');
                      },
                    ),
                    _buildDivider(),
                  ],
                ),
                const SizedBox(
                  height: 30.0,
                ),
                Text(
                  "App settings",
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: LOCAL_COLOR,
                  ),
                ),
                const SizedBox(height: 6.0),
                Column(
                  children: <Widget>[
                    Dark(),
                    SwitchListTile(
                      inactiveTrackColor: REMOTE_COLOR,
                      activeColor: LOCAL_COLOR,
                      contentPadding: const EdgeInsets.all(0),
                      value: false,
                      title: Text("Receive notifications"),
                      onChanged: null,
                    ),
                  ],
                ),
                const SizedBox(height: 42.0),
                Center(
                  child: RaisedButton(
                    padding: EdgeInsets.symmetric(horizontal: 98),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)),
                    onPressed: () {
                      rscontrol.stopRetroshare();
                      Navigator.pushReplacementNamed(context, '/login');
                    },
                    color: LOCAL_COLOR,
                    child: Text("SIGN OUT",
                        style: TextStyle(
                            fontSize: 16,
                            letterSpacing: 2.2,
                            color: Colors.white)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Container _buildDivider() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      width: double.infinity,
      height: 1.0,
      color: Colors.grey.shade400,
    );
  }
}

bool darkThemeEnabled = false;

class Dark extends StatefulWidget {
  const Dark({
    Key key,
  }) : super(key: key);

  @override
  _DarkState createState() => _DarkState();
}

class _DarkState extends State<Dark> {
  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeNotifier>(
      builder: (context, state, child) {
        return SwitchListTile(
          inactiveTrackColor: REMOTE_COLOR,
          activeColor: LOCAL_COLOR,
          contentPadding: const EdgeInsets.all(0),
          title: Text("Dark Theme"),
          value: state.isDarkThemeEnabled,
          onChanged: (_) {
            state.setDarkTheme(!state.isDarkThemeEnabled);
          },
        );
      },
    );
  }
}
