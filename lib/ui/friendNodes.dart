/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
 
import 'dart:async';

import 'package:elRepoIo/ui/bottommenu/pages/following_page.dart';
import 'package:elRepoIo/ui/common/updatableFutureBuilder.dart';
import 'package:elrepo_lib/models.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elrepo_lib/retroshare.dart' as rs;
import 'package:elrepo_lib/utils.dart' ;
import 'package:flutter/material.dart';

import '../constants.dart';

class FriendNodesScreen extends StatefulWidget {
  const FriendNodesScreen ({Key key}) : super(key: key);

  static Route<dynamic> route() => MaterialPageRoute(
        builder: (context) => FriendNodesScreen(),
      );

  @override
  _FriendNodesScreenState createState() => _FriendNodesScreenState();
}


class _FriendNodesScreenState extends State<FriendNodesScreen> {

  Timer _promiscuityTimer;
  bool _promisquity = true;

  UpdatabelFutureBuilderController friendsController = UpdatabelFutureBuilderController();

  Future<List<dynamic>> _getFriendList() async =>
        repo.getSslPeerList();


  Future<void> _checkPromiscuity(Timer t) async {
    if (t.isActive) {
      print("Executing promiscuity");
      var addedFriends = await repo.doPromiscuity();
      if (addedFriends.isNotEmpty) {
        print("Added new friends:" + addedFriends.toString());
        friendsController.runFuture();
        //TODO: fix snackbar caught error: Scaffold.of() called with a context that does not contain a Scaffold.
        String peernames = List.from(addedFriends.map((e) => e["mProfileName"])).join(",");
        final snackBar = SnackBar(
          content: Text("Added friends: $peernames"),
          duration: Duration(seconds: 6),
        );
        Scaffold.of(context).showSnackBar(snackBar);
      }
    }
  }

  void _setPromisquity(bool isPromiscuous) =>
    isPromiscuous ?
      _promiscuityTimer = Timer.periodic(
        Duration(seconds: 2),
        _checkPromiscuity,
      )
    :  _promiscuityTimer?.cancel();


  @override
  void dispose() {
    _promiscuityTimer?.cancel();
    super.dispose();
  }

  @override
  void initState() {
    _setPromisquity(_promisquity);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // todo: move all application AppBars to a model to DRY
      appBar: AppBar(
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.arrow_back_rounded,
            color: WHITE_COLOR,
          ),
        ),
        title: Text(
          'elRepo.io network',
          style: TextStyle(fontWeight: FontWeight.w800,
              color: WHITE_COLOR),
        ),
      ),
      body:

      NestedScrollView(
          headerSliverBuilder:
              (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                backgroundColor: Colors.white,
                  pinned: false,
              expandedHeight: 200,
              toolbarHeight: 80,
                flexibleSpace:
                    ListView(
                      shrinkWrap: true,
                      padding: EdgeInsets.all(15.0),
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 32,
                            vertical: 12,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SwitchListTile(
                                inactiveTrackColor: REMOTE_COLOR,
                                activeColor: LOCAL_COLOR,
                                contentPadding: const EdgeInsets.all(0),
                                value: _promisquity,
                                title: Text("Automatic peering"),
                                onChanged: (val) {
                                  setState(() {
                                    _promisquity = !_promisquity;
                                  });
                                  _setPromisquity(_promisquity);
                                },
                              ),
                              Text(
                                "If activated you will automatically connect with other elRepo.io nodes in your local network",
                                style: TextStyle(
                                  color: REMOTE_COLOR,
                                ),
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Divider(
                                height: 2,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Text(
                                'Known nodes',
                                style: TextStyle(
                                  fontSize: 20,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),

                ),
            ];
          },
          body:
          UpdatableFutureBuilder(
              generator: _getFriendList,
              controller: friendsController,
              callbackWidget: (context, data, updateFunction) =>
                  _peerCard(context, data, updateFunction)
          ),
      )
    );
  }
}

Widget _peerCard (context, dynamic data, onrerun) {
  return Card(
    elevation: 1.0,
    margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ListTile(
          leading:
          data["isConnected"] ?
            Icon(Icons.phonelink_setup, color: LOCAL_COLOR)
              : Icon(Icons.phonelink_erase, color: Colors.redAccent),
          title: Text(data["name"] + " - " + data["location"] ),
          subtitle: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Text(getFormatedTime(data["lastConnect"]))
              ),
              Text(data["id"]),
            ],
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            if (!data["isConnected"])
              TextButton(
                child:
                Row(
                  children: [
                    Icon(Icons.tap_and_play),
                    Text('Attempt connection'),
                  ],
                ),
                onPressed: () {
                  rs.RsPeers.connectAttempt(data["id"]);
                  onrerun();
                },
              ),
            const SizedBox(width: 8),
            TextButton(
              child:
                Row(
                  children: [
                    Icon(Icons.delete),
                    Text('Delete'),
                  ],
                ),
              onPressed: () {
                rs.RsPeers.removeFriend(data["gpg_id"]);
                onrerun();
              },
            ),
            const SizedBox(width: 8),
          ],
        ),
      ],
    ),
  );
}
