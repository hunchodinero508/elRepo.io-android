/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:elRepoIo/constants.dart';
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:elRepoIo/ui/common/updatableFutureBuilder.dart';
import 'package:flutter/material.dart';
import 'package:elRepoIo/ui.dart' show IdentityArguments;

class FollowingPage extends StatefulWidget {
  static Route<dynamic> route () => MaterialPageRoute(
    builder: (context) => FollowingPage(),
  );

  @override
  _FollowingPageState createState() => _FollowingPageState();
}

class _FollowingPageState extends State<FollowingPage> {

  Future<List<dynamic>> _getFollowedAuthorsDetails() =>
      repo.getFollowedAuthorsDetails();


  @override
  Widget build(BuildContext context) {
    return UpdatableFutureBuilder(
        generator: _getFollowedAuthorsDetails,
        callbackWidget: (context, data, updateFunction) =>
            _followingWidget(context, data)
    );
  }
}


Widget _followingWidget(context, dynamic data,) {
  return ListTile(
    title: InkWell(
        onTap: () {
          print("tapped on $data}");
          Navigator.pushNamed(context, "/identityDetails",
              arguments: IdentityArguments(
                  data["mId"]));
        },
        child: Text(data["mNickname"],
            style: TextStyle(color: REMOTE_COLOR))),
  );
}
