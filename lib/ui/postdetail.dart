/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of ui;

class PostDetailsScreen extends StatefulWidget {
  final PostArguments args;
  PostDetailsScreen(this.args);

  @override
  _PostDetailsScreenState createState() => _PostDetailsScreenState();
}

class _PostDetailsScreenState extends State<PostDetailsScreen> {
  Future<List> postDetails;
  String downloadingHash;
  bool fileExists = false;
  bool hasPayload = false;
  bool isBookmarked = false; // Used to know if a post without payload is bookmarked
  bool downloadQueued = false;
  String authorName = "";
  Future<List> postComments;

  _getPostDetails() => repo.getPostDetails(
      widget.args.forumId, widget.args.postId, widget.args.isLinkPost);

  @override
  void initState() {
    postDetails = _getPostDetails();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: WHITE_COLOR,
      appBar: AppBar(
        title: Text(
          "elRepo.io",
          style: TextStyle(
            color: WHITE_COLOR,
          ),
        ),
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
      ),
      body: postDetailsWidget(),
    );
  }

  Widget postDetailsWidget() {
    models.PostBody postBody = new models.PostBody();
    models.PostMetadata postMetadata;
    String postId;
    String forumId;
    IconData contentTypeIcon;

    return FutureBuilder(
      future: postDetails,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          print('Error: ${snapshot.error}');
          return Center(
            child: Text(IntlMsg.of(context).cannotLoadContent),
          );
        } else if (snapshot.hasData == false || snapshot.data.length == 0) {
          return loadingBox();
        } else {
          final data = snapshot.data[0];
          postId = data["mMeta"]["mMsgId"];
          try {
            postMetadata =
                models.PostMetadata.fromJsonString(data["mMeta"]["mMsgName"]);
          } catch (error) {
            print("Error parsing metadata Json: $error");
          }
          try {
            Map jsonBody = jsonDecode(data["mMsg"]);
            postBody.text = jsonBody["text"];
            if (jsonBody["payloadLinks"].length > 0) {
              Map linkData;
              for (linkData in jsonBody["payloadLinks"]) {
                postBody.payloadLinks.add(models.PayloadLink(
                    linkData["filename"], linkData["hash"], linkData["link"]));
              }
            }
          } catch (error) {
            print("Error parsing body Json: $error");
            // this may be a post originated from another client
            // we are choosing to show it but we could ignore it.
            postBody.text = data["mMsg"];
            postBody.payloadLinks = [];
          }
          final authorId = data["mMeta"]["mAuthorId"];
          final identityDetails = rs.RsIdentity.getIdDetails(authorId);
          identityDetails.then((details) {
            if (authorName == "") {
              if (mounted) setState(() => authorName = details["mNickname"]);
            }
          });

          if (widget.args.isLinkPost)
            forumId = data["mMeta"]["mGroupId"];
          else
            forumId = widget.args.forumId;

          if (postComments == null) {
            postComments = rs.RsGxsForum.getChildPosts(forumId, postId);
          }

          if (postBody.payloadLinks.length > 0) {
            hasPayload = true;
            contentTypeIcon = getContentTypeIcon(postMetadata.contentType);
          } else {
            // if there's no associated file, we consider the payload to be the inline text
            contentTypeIcon = Icons.format_align_left;
          }
          if (hasPayload) {
            final hash = postBody.payloadLinks[0].hash;
            final filePath = repo.getPathIfExists(hash);

            filePath.then((path) async {
              // the file is already available on this device
              if (path != null) {
                if (fileExists != true) {
                  if (mounted) setState(() => fileExists = true);
                }
              }
              // else check if it's being downloaded
              else {
                var hashes = await rs.RsFiles.fileDownloads();
                if (hashes.contains(hash)) {
                  downloadingHash = hash;
                  if (mounted) setState(() => downloadQueued = true);
                }
              }
            });
          }
          // If not have payload, check if is bookmarked (to display the icons correctly)
          else {
            data["mMeta"]["mAuthorId"] == rs.authIdentityId ?
              isBookmarked = true:
                repo.isBookmarked(forumId, postId).then(
                      (value) => setState(() => isBookmarked = value)
                );
          }

          return new Container(
              margin: defaultMargin(),
              child: new ListView(
                  children: <Widget>[
                    new ListTile(
                      leading: Container(
                          padding: EdgeInsets.only(right: 5.0),
                          decoration: new BoxDecoration(),
                          child: Icon(contentTypeIcon,
                              color: NEUTRAL_COLOR, size: 48)),
                      title: Text(
                          postMetadata.title != null ? postMetadata.title : "",
                          style: Theme.of(context).textTheme.subtitle1),
                      subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, "/identityDetails",
                                    arguments: IdentityArguments(authorId));
                              },
                              child: Text(authorName,
                                  style: TextStyle(color: LOCAL_COLOR)),
                            ),
                            Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  new IconButton(
                                      icon: new Icon(Icons.share,
                                          color: REMOTE_COLOR),
                                      onPressed: () async {
                                        String shareText =
                                            "${postMetadata.title}\n ${postMetadata.summary}";
                                        if (hasPayload && fileExists) {
                                          final filePath =
                                          await repo.getPathIfExists(
                                              postBody.payloadLinks[0].hash);
                                          Share.shareFiles([filePath],
                                              text: shareText);
                                        } else {
                                          Share.share(shareText);
                                        }
                                      }),
                                  if ((hasPayload && !fileExists)
                                      || (!isBookmarked && !fileExists))
                                    new DownloadWidget(
                                      postMetadata, postId,
                                      postBody, forumId,
                                      callback: () {
                                        postDetails = _getPostDetails();
                                        setState() {};
                                      })
                                  else if (hasPayload && fileExists)
                                    new OpenPayloadWidget(repo.getPathIfExists(
                                        postBody.payloadLinks[0].hash))
                                  else if (isBookmarked)
                                      new IconButton(
                                          icon: new Icon(Icons.star,
                                              color: REMOTE_COLOR)
                                      )].where(notNull).toList())
                          ]),
                    ),
                    Visibility(
                      // TODO(nicoechaniz): this works OK for showing the progressbar when visiting a content
                      // but when a download is started on a content, this does not get triggered and the bar is invisible
                        visible: (hasPayload &&
                            downloadQueued &&
                            downloadingHash != null),
                        child: DownloadBar(
                          downloadingHash: downloadingHash,
                          onDone: () {
                            if (fileExists != true) {
                              if (mounted) setState(() => fileExists = true);
                            }
                          }
                          ,)
                    ),
                    Container(
                        padding: defaultMargin(),
                        decoration: new BoxDecoration(
                            border: new Border(
                                top: new BorderSide(
                                    width: 1.0, color: Colors.black12))),
                        child: Text(
                          postBody.text,
                          style: Theme.of(context).textTheme.bodyText2,
                        )),
                    Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5.0),
                            child: RaisedButton(
                              onPressed: () {
                                print("Commenting on $forumId/$postId");
                                Navigator.pushNamed(context, "/comment",
                                    arguments: PostArguments(forumId, postId));
                              },
                              child: Text(IntlMsg.of(context).comment),
                              color: REMOTE_COLOR,
                              textColor: WHITE_COLOR,
                            ))),
                    (postComments != null)
                        ? new Container(
                        height: 300, child: postCommentsWidget(postComments))
                        : null
                  ].where(notNull).toList()));
        }
      },
    );
  }
}

class DownloadWidget extends StatefulWidget {
  final models.PostBody postBody;
  final models.PostMetadata postMetadata;
  final String postId;
  final String forumId;
  Function callback;
  DownloadWidget(this.postMetadata, this.postId, this.postBody, this.forumId,
      {this.callback});

  @override
  _DownloadWidgetState createState() => new _DownloadWidgetState();
}

class _DownloadWidgetState extends State<DownloadWidget> {
  Widget payloadActionIcon;
  Color statusColor;
  String hash;
  Future<Map> linksData;
  Future<String> filePath;
  bool hasPayload;

  @override
  void initState() {
    statusColor = REMOTE_COLOR;
    hasPayload = widget.postBody.payloadLinks.length > 0;
    if (hasPayload) {
      hash = widget.postBody.payloadLinks[0].hash;
      final postBody = widget.postBody;
      final link = postBody.payloadLinks[0].link;
      linksData = rs.RsFiles.parseFilesLink(link);
      filePath = repo.getPathIfExists(hash);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final postMetadata = widget.postMetadata;
    final postId = widget.postId;
    final forumId = widget.forumId;

    return new IconButton(
        icon: Icon(Icons.cloud_download, color: REMOTE_COLOR),
        onPressed: () async {
          print("Requesting file: $linksData");
          if(hasPayload) {
            linksData.then((data) {
              rs.RsFiles.requestFiles(data);
            });
          }
          //TODO(nicoechaniz): verify that the file is effectively requested for Download before creating the bookmark post.
          //TODO(kon): this logic should be moved to repo-lib
          String snackBarText;
          final bookmarksForumId =
            await repo.findOrCreateRepoForum("BOOKMARKS", rs.authIdentityId);
          print("Checking for bookmark in forum: $bookmarksForumId");
          final referred = "$forumId $postId";
          final bookmarkHeaders =
            await repo.getPostHeaders("BOOKMARKS", rs.authIdentityId);
          // TODO(nicoechaniz): find a more efficient way to do this
          List duplicates = bookmarkHeaders
              .where((i) => jsonDecode(i["mMsgName"])["referred"] == referred)
              .toList();
          if (duplicates.length == 0) {
            final linkPostMetadata =
            models.PostMetadata.generateLinkPostMetadata(
                postMetadata, postId, forumId);
            await rs.RsGxsForum.createPost(bookmarksForumId,
                json.encode(linkPostMetadata.toJson()), "", rs.authIdentityId);
          }
          snackBarText = IntlMsg.of(context).savingContentToLocalRepo;
          final snackBar = SnackBar(
            content: Text(snackBarText),
            duration: Duration(seconds: 5),
            onVisible: () => {},
          );
          Scaffold.of(context).showSnackBar(snackBar);
          if(widget.callback != null) widget.callback();
        });
  }
}

class OpenPayloadWidget extends StatefulWidget {
  final Future<String> filePath;
  OpenPayloadWidget(this.filePath);

  @override
  _OpenPayloadWidgetState createState() => new _OpenPayloadWidgetState();
}

class _OpenPayloadWidgetState extends State<OpenPayloadWidget> {
  @override
  Widget build(BuildContext context) {
    return new IconButton(
        icon: Icon(Icons.open_in_new, color: LOCAL_COLOR),
        onPressed: () {
          widget.filePath.then((path) async {
            if (path != null) {
              print("Opening file");
              // Check if is a network file
              if (path.startsWith(
                  rs.getRetroshareServicePrefix().substring(0, rs.getRetroshareServicePrefix().lastIndexOf("/"))
              )){
                String netPath = path;
                Directory tempDir = await getTemporaryDirectory();
                path = tempDir.path + "/" + path.split("/").last;

                print("Saving ${netPath} network file on: ${path}");

                await http.get(netPath).then((response) {
                  new File(path).writeAsBytes(response.bodyBytes);
                });

              }
              final openStatus = await OpenFile.open(path);
              print("Open file result: ${openStatus.message}");

            } else {}
          });
        });
  }
}


