/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */
 
 
import 'dart:async';

import 'package:elRepoIo/ui/common/postsTeaser.dart';
import 'package:elRepoIo/ui/common/updatableFutureBuilder.dart';
import 'package:flutter/material.dart';
import 'package:elrepo_lib/retroshare.dart' as rs;
import 'package:elrepo_lib/repo.dart' as repo;

import '../constants.dart';
import 'common/searchBar.dart';

import 'package:elRepoIo/ui.dart' show loadingBox;

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen>
    with TickerProviderStateMixin {
  
  // Text editing controller for the search bar
  TextEditingController _teController = TextEditingController();

  // Subscription to rsEvents
  StreamSubscription rsEvents;

  // Controll searching state
  bool _isSearching = false;

  // Used to store distant search results
  List<dynamic> resultsSearchList = [];
  List<Widget>  _createDistantList() {
    return new List<Widget>.generate(resultsSearchList.length, (int index) {
      return postTeaserCard(context, resultsSearchList[index]);
    });
  }

  Future<List<dynamic>> _distantSearchGenerator( ) async {
    rsEvents = await repo.distantSearch(_teController.text, (results) {
      repo.deleteFromPostList(resultsSearchList, results);
      for(var result in results) {
//            result["mMsgName"] = "Blaalalaa";
        resultsSearchList.add(result);
      }
      setState(() {});
    });
  }

  Future<void> searchAction(TextEditingController _teController) async {
    setState((){
      _isSearching = true;
    });
    resultsSearchList = await repo.sortedLocalSearch(_teController.text);
    _distantSearchGenerator();
    setState((){
      _isSearching = false;
    });
  }

  @override
  void dispose() {
    rsEvents?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.arrow_back_rounded,
            color: WHITE_COLOR,
          ),
        ),
        title: Text(
          'Search',
          style: TextStyle(fontWeight: FontWeight.w800,
              color: WHITE_COLOR),
        ),
      ),
      body: Column(
        children: [
          Container(
            child: SearchBar(
              teController: _teController,
              searchAction: searchAction,
            )
          ),
          SizedBox(
            height: 16,
          ),
          Visibility(
            visible: _isSearching,
            child: loadingBox()
          ),
          Visibility(
              visible: !_isSearching && resultsSearchList.isEmpty,
              child: Text("No results")
          ),
          Visibility(
            visible: !_isSearching,
            child: Expanded(
              child:
              ListView(
                  children:
                  [
                    for(var distantTeaser in _createDistantList()) distantTeaser
                  ]
              ),
            ),
          ),

        ],
      ),
    );
  }

}

