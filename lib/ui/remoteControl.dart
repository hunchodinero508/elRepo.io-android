/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

part of ui;

class RemoteControlScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RemoteControl();
  }
}

class RemoteControl extends StatefulWidget {
  @override
  _RemoteControlState createState() => _RemoteControlState();
}

class _RemoteControlState extends State<RemoteControl> {
  final prefixUrlETController = TextEditingController();

  bool _errorMsgVisible = false;
  String _prefixInfoText;

  String _isLocallyConnected = "not connected to a remote elRepo.io node";
  String _isRemoteControl = "Connected to: ";

  @override
  void initState() {
    _prefixInfoText = _getPrefixInfoText();
    super.initState();
  }

  // Info text that tell if you are connected local o remotelly
  String _getPrefixInfoText() =>
      rs.getRetroshareServicePrefix() == rs.RETROSHARE_SERVICE_PREFIX
          ? _isLocallyConnected
          : _isRemoteControl + rs.getRetroshareServicePrefix();

  // Error message when can't connect to remote
  void _showErrorMessage(bool show) {
    setState(() {
      _errorMsgVisible = show;
    });
  }

  void _setRemoteControl() {
    _showErrorMessage(false);
    setState(() {
      _prefixInfoText = _getPrefixInfoText();
    });
  }

  void _disconnectRemoteControl() {
    repo.disableRemoteControl();
    setState(() {
      _prefixInfoText = _getPrefixInfoText();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        backgroundColor: PURPLE_COLOR.withOpacity(0.9),
        shadowColor: REMOTE_COLOR,
        brightness: Brightness.dark,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
        leading: IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: Icon(
            Icons.arrow_back_rounded,
            color: WHITE_COLOR,
          ),
        ),
        title: Text(
          "Remote Control",
          style: TextStyle(
            fontWeight: FontWeight.w800,
            color: WHITE_COLOR,
          ),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 32,
                vertical: 12,
              ),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      _prefixInfoText,
                      style: TextStyle(
                        color: ACCENT_COLOR,
                      ),
                    ),
                    TextField(
                      controller: prefixUrlETController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        hintText: "http://localhost:8888/rsremote",
                        icon: Icon(Icons.settings_remote),
                      ),
                    ),
                    SizedBox(
                      height: 32,
                    ),
                    Text(
                      "You need to specify the complete URL to your remote node control API.",
                      style: TextStyle(
                        color: REMOTE_COLOR,
                      ),
                    ),
                    SizedBox(
                      height: 32,
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            child: new RaisedButton(
                              color: LOCAL_COLOR,
                              disabledColor: ACCENT_COLOR,
                              elevation: 4.0,
                              splashColor: REMOTE_COLOR,
                              onPressed: () async {
                                await repo.enableRemoteControl(
                                        prefixUrlETController.text,
                                        rs.RETROSHARE_API_USER)
                                    ? _setRemoteControl()
                                    : _showErrorMessage(true);
                              },
                              child: new Text(
                                "Conecta",
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 32,
                          ),
                          Container(
                            child: new RaisedButton(
                              color: LOCAL_COLOR,
                              disabledColor: ACCENT_COLOR,
                              elevation: 4.0,
                              splashColor: REMOTE_COLOR,
                              onPressed: () async {
                                _disconnectRemoteControl();
                              },
                              child: new Text(
                                "Desconecta",
                              ),
                            ),
                          ),
                        ]),
                    Visibility(
                      visible: _errorMsgVisible,
                      child: Text(
                        "ERROR: No se pudo conectar al nodo remoto!",
                        style: TextStyle(
                          color: Colors.redAccent,
                        ),
                      ),
                    )
                  ]),
            ),
          ),
        ),
      ),
    ));
  }
}
