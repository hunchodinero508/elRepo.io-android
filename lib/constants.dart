/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:flutter/material.dart';

const APP_DIR = 'net.altermundi.elrepoio';
const API_VERSION = "0.0.13";
const FORUM_PREPEND = "elRepo.io_";
const SUMMARY_LENGTH = 100;
//final BACKGROUND_COLOR = Colors.green[50];
final BACKGROUND_COLOR = Color(0xffe5ffe4);
final WHITE_COLOR = Color(0xffffffff);
//const LOCAL_COLOR = Colors.green;
const LOCAL_COLOR = Color(0xffa5c461);
const ACCENT_COLOR = Color(0xffb3cd79);
const REMOTE_COLOR = Color(0xff895978);
//const REMOTE_COLOR = Color(0xff8a3444);
const PURPLE_COLOR = Color(0xff895978);
const NEUTRAL_COLOR = Color(0xff898989);
const BTN_FONT_COLOR = Color(0xff101b00);

class AccountStatus {
  static const String isLoggedIn = "is logged in";
  static const String hasLocation = "has location";
  static const String hasNoLocation = "has no location";
}

class MarkupTypes {
  static const String plain = "plain";
  static const String markdown = "markdown";
  static const String html = "html";
}

class ContentTypes {
  static const String ref = "ref";
  static const String image = "image";
  static const String audio = "audio";
  static const String video = "video";
  static const String document = "document";
  static const String text = "text";
  static const String file = "file";
}

class PostRoles {
  static const String post = "post";
  static const String comment = "comment";
}
