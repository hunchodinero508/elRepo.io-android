/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'dart:ui';

import 'package:elRepoIo/constants.dart' as cnst;
import 'package:elRepoIo/ui.dart';
import 'package:elRepoIo/ui/bottommenu/pages/tabs_page.dart';
import 'package:elRepoIo/ui/friendNodes.dart';
import 'package:elRepoIo/ui/search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'dart:async';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:sentry/sentry.dart';
import 'package:connectivity/connectivity.dart';

import 'constants.dart';
import 'rscontrol.dart' as rsControl;
import 'package:elrepo_lib/repo.dart' as repo;
import 'ui.dart' as ui;
import 'utils.dart' as utils;
import 'intl.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

User user = new User();

final SentryClient _sentry = SentryClient(
    dsn:
        "https://f3dadd9fc2324d8fbf3655bfafc3895c@o448455.ingest.sentry.io/5429863");

bool get isInDebugMode {
  // Assume you're in production mode.
  bool inDebugMode = false;

  // Assert expressions are only evaluated during development. They are ignored
  // in production. Therefore, this code only sets `inDebugMode` to true
  // in a development environment.
  assert(inDebugMode = true);

  return inDebugMode;
}

Future<void> _reportError(dynamic error, dynamic stackTrace) async {
  // Print the exception to the console.
  print('Caught error: $error');
  if (isInDebugMode) {
    // Print the full stacktrace in debug mode.
    print(stackTrace);
  } else {
    // Print the full stacktrace
    print(stackTrace);
    // and send the Exception and Stacktrace to Sentry in Production mode.
    _sentry.captureException(exception: error, stackTrace: stackTrace);
  }
}


void main() async {
  runZonedGuarded<Future<void>>(
    () async {
      // TODO(nicoechaniz): propperly dispose of the subscription on app close
      StreamSubscription<ConnectivityResult> connectivitySubscription;
      final Connectivity _connectivity = Connectivity();

      WidgetsFlutterBinding.ensureInitialized();
      connectivitySubscription =
          _connectivity.onConnectivityChanged.listen(utils.updateConnectionStatus);

      runApp(ChangeNotifierProvider<ThemeNotifier>(
          create: (_) => ThemeNotifier(),
          child: Consumer<ThemeNotifier>(
            builder: (context, state, child) {
              return MaterialApp(
                localizationsDelegates: [
                  const SimpleLocalizationsDelegate(),
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                ],
                supportedLocales: [
                  const Locale('en', ''),
                  const Locale('es', ''),
                ],
                theme: state.currentTheme,
                initialRoute: '/',
                routes: {
                  '/': (context) => SplashScreen(),
                  '/signup': (context) => ui.SignUpScreen(),
                  '/onboarding': (context) => ui.OnBoarding(),
                  '/login': (context) => ui.LoginScreen(),
                  '/friendnodes': (context) => FriendNodesScreen(),
                  '/browsex': (context) => ui.BrowseXScreen(),
                  '/createbackup': (context) => ui.BackupDownloading(),
                  '/settings': (context) => ui.SettingsProfile(),
                  '/search': (context) => SearchScreen(),
                  '/editprofile': (context) => ui.EditMyProfile(),
                  '/remotecontrol': (context) => ui.RemoteControl(),
                  '/main': (context) => DefaultTabController(
                        length: 3,
                        child: Scaffold(
                          appBar: AppBar(
                            centerTitle: true,
                            backgroundColor: PURPLE_COLOR.withOpacity(0.9),
                            shadowColor: REMOTE_COLOR,
                            brightness: Brightness.dark,
                            toolbarOpacity: 1.0,
                            title: Text(
                              'elRepo.io',
                              style:
                                  TextStyle(color: WHITE_COLOR),
                            ),
                            elevation: 3,
                            actions: <Widget>[
                              IconButton(
                                icon: Icon(
                                  Icons.search,
                                  color: WHITE_COLOR,
                                ),
                                onPressed: () {
                                  Navigator.pushNamed(context, '/search');
                                },
                              ),
                              IconButton(
                                icon: Icon(
                                  Icons.settings,
                                  color: WHITE_COLOR,
                                ),
                                onPressed: () {
                                  //Navigator.pop(context);
                                  Navigator.pushNamed(context, '/settings');
                                },
                              ),
                            ],
                          ),
                  body: TabsPage(),
                ),
              ),
        },
        onGenerateRoute: (RouteSettings settings) {
          print('build route for ${settings.name} using ${settings.arguments}');
          var routes = <String, WidgetBuilder>{
            "/postdetails": (context) =>
                ui.PostDetailsScreen(settings.arguments),
            "/browsetag": (context) => ui.BrowseTagScreen(settings.arguments),
            "/comment": (context) => ui.CommentScreen(settings.arguments),
            "/identityDetails": (context) =>
                ui.IdentityDetailsScreen(settings.arguments),
          };
          WidgetBuilder builder = routes[settings.name];

          return MaterialPageRoute(builder: (context) => builder(context));
        },
      );})));
    },
    (Object error, StackTrace stackTrace) {
      _reportError(error, stackTrace);
    },
  );
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future<List> postHeaders;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  @override
  void initState() {
    super.initState();
    var initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings);
    _showKeepAliveNotification();

    utils.getOrCreateExternalDir();

    rsControl.startRetroshare().then((isRunning) {
      if (isRunning) {
        redirectOnAccountStatus(context);
      } else {
        // TODO(nicoechaniz): expose the failure to start RS service to the user interface
        throw Exception("RS is not running");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Center(child: Image.asset("assets/logo.png")),
          Text(
            "elRepo.io\n",
            style: Theme.of(context).textTheme.display1,
          ),
        ],
      ),
    );
  }

  Future _showKeepAliveNotification() async {
    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'elRepo.io',
        'keep alive notification',
        'this notification keeps elRepo.io alive',
        playSound: false,
        importance: Importance.max,
        priority: Priority.max);
    var iOSPlatformChannelSpecifics =
        new IOSNotificationDetails(presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
      0,
      'elRepo.io',
      'this notification keeps elRepo.io alive',
      platformChannelSpecifics,
      payload: 'this is the payload',
    );
  }
}

void redirectOnAccountStatus(BuildContext context) async {
  final accountStatus = await repo.getAccountStatus();
  switch (accountStatus) {
    case cnst.AccountStatus.isLoggedIn:
      Navigator.pushReplacementNamed(context, '/main');
      break;
    case cnst.AccountStatus.hasLocation:
      Navigator.pushReplacementNamed(context, '/login');
      break;
    case cnst.AccountStatus.hasNoLocation:
      Navigator.pushReplacementNamed(context, '/signup');
      break;
  }
}
