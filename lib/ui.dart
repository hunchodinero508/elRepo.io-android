/*
 * elRepo.io decentralized culture repository
 *
 * Copyright (C) 2019-2021  Asociación Civil Altermundi <info@altermundi.net>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-FileCopyrightText:  2019-2021 Asociación Civil Altermundi <info@altermundi.net>
 * SPDX-License-Identifier: AGPL-3.0-only
 */

library ui;

import 'dart:io';

import 'package:elRepoIo/rscontrol.dart';
import 'package:elRepoIo/ui/common/downloadBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'package:elrepo_lib/retroshare.dart' as rs;
import 'package:elRepoIo/rscontrol.dart' as rscontrol;
import 'package:provider/provider.dart';
import 'dart:convert';
import 'utils.dart' as utils;
import 'package:elrepo_lib/repo.dart' as repo;
import 'package:share/share.dart';
import 'package:flutter_file_dialog/flutter_file_dialog.dart' as fileDialog;
import 'package:elrepo_lib/models.dart' as models;
import 'package:path/path.dart' as path;
import 'constants.dart';
import 'package:open_file/open_file.dart';
import 'intl.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dio/dio.dart';
import 'package:flutter_archive/flutter_archive.dart' as archive;
import 'package:http/http.dart' as http;
import 'ui/common/postsTeaser.dart';
import 'ui/bottommenu/pages/following_page.dart';
import 'package:elRepoIo/constants.dart' as cnst;

part "ui/postdetail.dart";
part "ui/publish.dart";
part "ui/login.dart";
part "ui/signup.dart";
part "ui/browsetag.dart";
part "ui/comment.dart";
part "ui/identityDetails.dart";
part "ui/editprofile.dart";
part "ui/settings.dart";
part "ui/profile.dart";
part "ui/onboarding.dart";
part 'ui/browseTabBar.dart';
part 'ui/createBackup.dart';
part 'ui/themeState.dart';
part 'ui/remoteControl.dart';

bool notNull(Object o) => o != null;

class PostArguments {
  final String forumId;
  final String postId;
  final bool isLinkPost;
  PostArguments(this.forumId, this.postId, [this.isLinkPost]);
}

class TagArguments {
  final String tagName;
  TagArguments(this.tagName);
}

class IdentityArguments {
  final String identityId;
  IdentityArguments(this.identityId);
}

Widget loadingBox([String loadingMsg = 'Loading ...']) {
  return Center(
    child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            child: CircularProgressIndicator(),
            width: 60,
            height: 60,
          ),
          new Padding(
            padding: EdgeInsets.only(top: 15),
            child: Text(loadingMsg),
          )
        ]),
  );
}

Future showLoadingDialog(context, [String loadingMsg = 'Loading ...']) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return SimpleDialog(
        children: <Widget>[
          Center(
              child: Column(children: [
            CircularProgressIndicator(),
            SizedBox(
              height: 10,
            ),
            Text(loadingMsg, style: TextStyle(color: ACCENT_COLOR))
          ]))
        ],
      );
    },
  );
}

defaultMargin() {
  return EdgeInsets.all(15.0);
}

Future<bool> isBookmarked(msgId) async {
  final bookmarkedIds = await repo.getBookmarkedIds();
  if (bookmarkedIds != null && bookmarkedIds.contains(msgId))
    return true;
  else
    return false;
}

Future<bool> isSelfPost(msgId) async {
  final selfPostIds = await repo.getSelfPostIds();
  if (selfPostIds != null && selfPostIds.contains(msgId))
    return true;
  else
    return false;
}

Future<bool> isLocal(msgId) async {
  // skip this check, until we do an efficient implementation
  return false;
  return (await (isBookmarked(msgId)) || await (isSelfPost(msgId)));
}

Widget getPostIcon(models.PostMetadata postMetadata, msgId,
    [bool isLinkPost = false]) {
  Icon postIcon;
  var realMsgId;

  if (isLinkPost) {
    realMsgId = repo.getRealPostId(postMetadata);
  } else {
    realMsgId = msgId;
  }

  return FutureBuilder(
    future: isLocal(realMsgId),
    builder: (context, snapshot) {
      if (snapshot.hasError) {
        print('Error: ${snapshot.error}');
        return Center(
          child: Text(IntlMsg.of(context).cannotLoadContent),
        );
      } else if (snapshot.hasData == false) {
        return Icon(Icons.hourglass_empty, color: NEUTRAL_COLOR);
      }
      switch (snapshot.data) {
        case (true):
          {
            postIcon = Icon(getContentTypeIcon(postMetadata.contentType),
                color: LOCAL_COLOR);
            break;
          }
        case (false):
          postIcon = Icon(getContentTypeIcon(postMetadata.contentType),
              color: REMOTE_COLOR);
      }
      return postIcon;
    },
  );
}


IconData getContentTypeIcon(String contentType) {
  if (contentTypeIcons.containsKey(contentType)) {
    return contentTypeIcons[contentType];
  } else {
    // the default icon is the file icon for unknown types
    return contentTypeIcons[models.ContentTypes.file];
  }
}

Map<String, IconData> contentTypeIcons = {
  models.ContentTypes.image: Icons.image,
  models.ContentTypes.audio: Icons.audiotrack,
  models.ContentTypes.video: Icons.local_movies,
  models.ContentTypes.document: Icons.insert_drive_file,
  models.ContentTypes.file: Icons.attach_file,
  models.ContentTypes.text: Icons.format_align_left
};
